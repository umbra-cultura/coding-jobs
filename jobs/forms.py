from django import forms
from .models import Job, Application

class addJobForm(forms.ModelForm):
    class Meta:
        model = Job
        fields = ['titulo', 'descricao_curta', 'descricao_longa', 'nome_companhia', 'endereco_companhia', 'cep_companhia', 'cidade_companhia', 'pais_companhia', 'tamanho_companhia']

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ['conteudo', 'experiencia']