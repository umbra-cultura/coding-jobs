from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from notifications.utilities import create_notification
from .models import Job
from .forms import addJobForm, ApplicationForm

# Create your views here.
def search(request):
    return render(request, 'jobs/search.html')

def job_detail(request, job_id):
    job = Job.objects.get(pk=job_id)

    return render(request, 'jobs/job_detail.html', { 'job': job })

@login_required
def apply_for_job(request, job_id):
    job = Job.objects.get(pk=job_id)
    if request.method == 'POST':
        form = ApplicationForm(request.POST)
        if form.is_valid():
            application = form.save(commit=False)
            application.trabalho = job
            application.criado_por = request.user
            application.save()

            create_notification(request, job.criado_por, 'application', application.id)

            return redirect('dashboard')
    else:
        form = ApplicationForm()

    return render(request, 'jobs/apply_for_job.html', {'form': form, 'job': job})

@login_required
def add_job_request(request):
    if request.method == 'POST':
        form = addJobForm(request.POST)
        if form.is_valid():
            job = form.save(commit=False)
            job.criado_por = request.user
            job.save()

            return redirect('dashboard')

    else:
        form = addJobForm()

    return render(request, 'jobs/add_job.html', {'form': form})

@login_required
def edit_job_request(request, job_id):
    job = get_object_or_404(Job, pk=job_id, criado_por=request.user)

    if request.method == 'POST':
        form = addJobForm(request.POST, instance=job)
        if form.is_valid():
            job = form.save(commit=False)
            job.status = request.POST.get('status')
            job.save()

            return redirect('dashboard')

    else:
        form = addJobForm(instance=job)

    return render(request, 'jobs/edit_job.html', {'form': form, 'job': job})