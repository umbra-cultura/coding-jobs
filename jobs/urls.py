from django.urls import path, include

from .views import job_detail, add_job_request, edit_job_request, apply_for_job, search
from .api import api_search

urlpatterns = [
    path('api/busca/', api_search, name='api_search'),
    path('busca/', search, name='search'),
    path('add/', add_job_request, name='add_job_request'),
    path('<int:job_id>/edit/', edit_job_request, name='edit_job_request'),
    path('<int:job_id>/', job_detail, name='job_detail'),
    path('<int:job_id>/apply/', apply_for_job, name='apply_for_job'),
]