import json

from django.db.models import Q
from django.http import JsonResponse

from .models import Job

def api_search(request):
    jobslist = []
    data = json.loads(request.body)
    query = data['query']
    tamanho_companhia = data['tamanho_companhia']

    jobs = Job.objects.filter(status=Job.ATIVO).filter(Q(titulo__icontains=query) | Q(descricao_curta__icontains=query) | Q(descricao_longa__icontains=query) | Q(nome_companhia__icontains=query) | Q(cidade_companhia__icontains=query))
    if tamanho_companhia:
        jobs = jobs.filter(tamanho_companhia=tamanho_companhia)

    for job in jobs:
        obj = {
            'id': job.id,
            'titulo': job.titulo,
            'nome_companhia': job.nome_companhia,
            'url': '/jobs/%s' % job.id
        }
        jobslist.append(obj)
    return JsonResponse({'jobs': jobslist})