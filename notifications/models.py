from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Notification(models.Model):
    MESSAGE = 'message'
    APPLICATION = 'application'

    CHOICES = (
        (MESSAGE, 'Message'),
        (APPLICATION, 'Application')
    )

    para_user = models.ForeignKey(User, related_name='notifications', on_delete=models.CASCADE)
    tipo_notificacao = models.CharField(max_length=20, choices=CHOICES)
    lido = models.BooleanField(default=False)
    extra_id = models.IntegerField(null=True, blank=True)
    criado_em = models.DateTimeField(auto_now_add=True)
    criado_por = models.ForeignKey(User, related_name='created_notifications', on_delete=models.CASCADE)

    class Meta:
        ordering: ['-criado_em']