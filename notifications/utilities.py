from .models import Notification

def create_notification(request, para_user, tipo_notificacao, extra_id=0):
    notification = Notification.objects.create(para_user=para_user, tipo_notificacao=tipo_notificacao, criado_por=request.user, extra_id=extra_id)