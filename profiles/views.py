from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from notifications.utilities import create_notification
from jobs.models import Job, Application
from .models import Messages

# Create your views here.
@login_required
def dashboard(request):
    return render(request, 'profiles/dashboard.html', { 'profile': request.user.userprofile })

@login_required
def view_application(request, application_id):
    if request.user.userprofile.is_employer:
        application = get_object_or_404(Application, pk=application_id, trabalho__criado_por=request.user)
        para_user = application.criado_por
    else:
        application = get_object_or_404(Application, pk=application_id, criado_por=request.user)
        para_user = application.trabalho.criado_por
    if request.method == 'POST':
        conteudo = request.POST.get('conteudo')
        if conteudo:
            message = Messages.objects.create(aplicacao=application, conteudo=conteudo, criado_por=request.user)
            create_notification(request, para_user, 'message', application.id)
            return redirect('view_application', application_id=application.id)
    return render(request, 'profiles/view_application.html', {'application': application})

@login_required
def view_dashboard_job(request, job_id):
    job = get_object_or_404(Job, pk=job_id, criado_por=request.user)
    return render(request, 'profiles/view_dashboard_job.html', {'job': job})