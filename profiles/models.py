from django.contrib.auth.models import User
from django.db import models
from jobs.models import Application

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, related_name='userprofile', on_delete=models.CASCADE)
    is_employer = models.BooleanField(default=False)

User.userprofile = property(lambda u:Profile.objects.get_or_create(user=u)[0])

class Messages(models.Model):
    aplicacao = models.ForeignKey(Application, related_name='messages', on_delete=models.CASCADE)
    conteudo = models.TextField()
    criado_por = models.ForeignKey(User, related_name='messages', on_delete=models.CASCADE)
    criado_em = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['criado_em']